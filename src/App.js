import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import LandingPage from './container/LandingPage/LandingPage';
import ListProductPage from './container/ListProductPage/ListProductPage';
import ProductDetailPage from './container/ProductDetailPage/ProductDetailPage';

function App() {
  return (
    <Router>
      <div className="App container">
        <Switch>
          <Route path="/" exact component={LandingPage} />
          <Route path="/ListProduct" component={ListProductPage} />
          <Route path="/ProductDetail" component={ProductDetailPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
