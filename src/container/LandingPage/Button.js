import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

function Button(props) {
  const classes = useStyles();
  const shoot = () => {
    const { username, password } = props;
    if (username === '') {
      alert('please input username');
    }
    if (username === 'Admin' && password === '1234') {
      console.log(2);

      const location = {
        pathname: '/ListProduct',
        state: { username: username },
      };
      props.history.push(location);
    } else {
      alert('wrong password !');
    }
  };
  return (
    <div className={classes.root}>
      <Fab
        variant="extended"
        onClick={shoot}
        style={{
          backgroundColor: '#fff',
          color: '#3CC2A8',
          borderRadius: 10,
          fontWeight: 'bold',
        }}
      >
        <NavigationIcon className={classes.extendedIcon} />
        Sign In
      </Fab>
    </div>
  );
}
export default withRouter(Button);
