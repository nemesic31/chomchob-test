import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import Button from './Button';
import TextField from '@material-ui/core/TextField';

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }
  render() {
    return (
      <div>
        <p style={{ padding: 30 }} />
        <img
          style={{ padding: 10, width: '20%', height: '20%' }}
          src={require('./logochomchob.jpg')}
        />

        {/* username */}
        <form noValidate autoComplete="off" style={{ padding: 10 }}>
          <TextField
            style={{
              backgroundColor: '#3CC2A8',
              borderRadius: 10,
            }}
            label="Username"
            variant="outlined"
            onChange={(event) =>
              this.setState({ username: event.target.value })
            }
          />
          <p />
          {/* password */}
          <TextField
            style={{ backgroundColor: '#3CC2A8', borderRadius: 10 }}
            label="Password"
            variant="outlined"
            onChange={(event) =>
              this.setState({ password: event.target.value })
            }
            type="password"
          />
        </form>

        <Button username={this.state.username} password={this.state.password} />
      </div>
    );
  }
}
export default withRouter(LandingPage);
