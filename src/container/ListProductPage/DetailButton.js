import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

function DetailButton(props) {
  const classes = useStyles();
  const shoot = () => {
    const { nameProduct, imgProduct, detailProduct, priceProduct } = props;
    // const { imgProduct } = props;
    // const { detailProduct } = props;
    // const { priceProduct } = props;
    const location = {
      pathname: '/ProductDetail',
      state: {
        nameProduct: nameProduct,
        imgProduct: imgProduct,
        detailProduct: detailProduct,
        priceProduct: priceProduct,
      },
    };
    props.history.push(location);
  };
  return (
    <div className={classes.root}>
      <Fab
        variant="extended"
        onClick={shoot}
        style={{
          backgroundColor: '#3CC2A8',
          color: '#fff',
          borderRadius: 10,
          fontWeight: 'bold',
        }}
      >
        Detail
      </Fab>
    </div>
  );
}
export default withRouter(DetailButton);
