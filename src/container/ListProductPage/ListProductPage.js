import React, { Component } from 'react';
import { Card } from '@material-ui/core';
import { withRouter, Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import ListIcon from '@material-ui/icons/List';
import GridTcon from '@material-ui/icons/Apps';
import DetailButton from './DetailButton';
import styled from 'styled-components';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

export default class ListProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],

      count: 0,
      nameProduct: '',
      detailProduct: '',
      priceProduct: '',
      imgProduct: '',
      // password: '',
    };
  }

  componentDidMount() {
    fetch('https://cc-mock-api.herokuapp.com/product')
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
        this.setState({ details: response });
      });
  }

  gridView() {
    function changeBackground(e) {
      e.target.style.background = '#B4F1D7';
    }
    function changeBackground2(a) {
      a.target.style.background = '#fff';
    }
    return (
      <div>
        {this.state.details.map((detail) => (
          <Card
            style={{
              width: '15rem',
              display: 'inline-block',
              marginLeft: 5,
              marginRight: 5,
              marginBottom: 10,
            }}
            onMouseEnter={changeBackground}
            onMouseLeave={changeBackground2}
          >
            <div class="Card">
              <img
                style={{ width: 200, height: 200 }}
                src={`${detail.image_url}`}
              />

              <h5>{detail.name.substr(0, 19)}</h5>
              <h5>{detail.description.substr(0, 100)}</h5>
              <h5 style={{ color: 'red', textAlign: 'end', marginRight: 13 }}>
                ${detail.price}
              </h5>
              <DetailButton
                nameProduct={detail.name}
                imgProduct={detail.image_url}
                detailProduct={detail.description}
                priceProduct={detail.price}
              />
            </div>
          </Card>
        ))}
      </div>
    );
  }
  listView() {
    function changeBackground(e) {
      e.target.style.background = '#B4F1D7';
    }
    function changeBackground2(a) {
      a.target.style.background = '#fff';
    }
    return (
      <div style={{ marginLeft: '15%', marginRight: '15%' }}>
        {this.state.details.map((detail) => (
          <List className={useStyles.root}>
            <ListItem
              alignItems="center"
              onMouseEnter={changeBackground}
              onMouseLeave={changeBackground2}
            >
              <ListItemAvatar>
                <img
                  style={{ width: 200, height: 200, display: 'inline-block' }}
                  src={`${detail.image_url}`}
                />
              </ListItemAvatar>
              <ListItemText
                primary={detail.name}
                secondary={
                  <React.Fragment>
                    <Typography
                      component="span"
                      variant="body2"
                      className={useStyles.inline}
                      color="textPrimary"
                    >
                      <p style={{ marginRight: 40, marginLeft: 10 }}>
                        {detail.description}
                      </p>
                    </Typography>
                  </React.Fragment>
                }
              />
              <div>
                <h5 style={{ color: 'red', textAlign: 'end', marginRight: 20 }}>
                  ${detail.price}
                </h5>
                <DetailButton
                  nameProduct={detail.name}
                  imgProduct={detail.image_url}
                  detailProduct={detail.description}
                  priceProduct={detail.price}
                />
              </div>
            </ListItem>
          </List>
        ))}
      </div>
    );
  }
  render() {
    return (
      <div>
        <div>
          <h5 style={{ textAlign: 'end', marginRight: 100 }}>
            {/* {this.props.location.state.username} */}

            <ButtonGroup
              disableElevation
              variant="outlined"
              color="inherit"
              borderColor="green"
            >
              <Button
                style={{ borderColor: 'green' }}
                onClick={() => this.setState({ count: 0 })}
              >
                <GridTcon />
              </Button>
              <Button
                style={{ borderColor: 'green' }}
                onClick={() => this.setState({ count: 1 })}
              >
                <ListIcon />
              </Button>
            </ButtonGroup>
          </h5>

          <h1>รายการสินค้า</h1>
        </div>

        {this.state.count === 0 ? this.gridView() : this.listView()}
      </div>
    );
  }
}
