import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import { Card } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

export default class ProductDetailPage extends Component {
  render() {
    const {
      priceProduct,
      nameProduct,
      imgProduct,
      detailProduct,
    } = this.props.location.state;
    return (
      <div>
        <div>
          <h1>รายละเอียดสินค้า</h1>
        </div>
        <div style={{ marginLeft: '10%', marginRight: '10%' }}>
          <List className={useStyles.root}>
            <ListItem alignItems="center">
              <ListItemAvatar>
                <img
                  style={{ width: 400, height: 400, display: 'inline-block' }}
                  src={imgProduct}
                />
              </ListItemAvatar>
              <ListItemText
                primary={nameProduct}
                secondary={
                  <React.Fragment>
                    <Typography
                      component="span"
                      variant="body2"
                      className={useStyles.inline}
                      color="textPrimary"
                    >
                      <p style={{ marginRight: 40, marginLeft: 10 }}>
                        {detailProduct}
                      </p>
                      <div>
                        <h5
                          style={{
                            color: 'red',

                            marginLeft: 20,
                          }}
                        >
                          ${priceProduct}
                        </h5>

                        <button
                          style={{
                            marginRight: 20,
                            backgroundColor: '#3CC2A8',

                            height: 40,
                            borderRadius: 5,
                            color: '#fff',
                            fontSize: 15,
                            width: 150,
                          }}
                        >
                          <b>Add to cart</b>
                        </button>
                      </div>
                    </Typography>
                  </React.Fragment>
                }
              />
            </ListItem>
          </List>
        </div>
      </div>
    );
  }
}
